teach_flow = 
	[
	{
		 tip:"选择钥匙,氢氧化钠，表面皿点击确定，开始实验.",
		 tag:[TAG_LIB,
		      TAG_LIB_BG8,
		      TAG_LIB_SURFACE,      
		      ]
	 },

	 {
		 tip:"取出氢氧化钠试剂瓶瓶塞.",
		 tag:[
		      TAG_SURFACE,
              
		      TAG_BOTTLE3_LID
		      ],action:ACTION_DO1
	 },
	 

	 {
		 tip:"使用钥匙取出氢氧化钠放入表面皿中",
		 tag:[TAG_SURFACE,  TAG_SPOON ]
	 },
	 {
		 tip:"合上瓶塞等待若干时间观察氢氧化钠潮解现象.",
		 tag:[
		      TAG_SURFACE,

		      TAG_BOTTLE3_LID
		      ],action:ACTION_DO2
	 },
//	 {
//		 tip:"选择钥匙,氢氧化钠，表面皿点击确定，开始实验.",
//		 tag:[TAG_LIB1,
//		      TAG_LIB_BG8,
//		      TAG_LIB_SURFACE,      
//		      ]
//	 },


	 {
		 tip:"恭喜过关.",
		 over:true
	 }
	 ]