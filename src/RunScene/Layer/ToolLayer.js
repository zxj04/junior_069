var ToolLayer = cc.Layer.extend({
	scene:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 20);
		this.init();
	},
	init: function (){
		var lib = new  ButtonScale(this,"#button/lib.png",this.callback);
		lib.setTag(TAG_BUTTON_LIB);
		lib.setPosition(cc.p(30 + lib.width*0.5, 39 + lib.height * 0.5));
	},
	callback: function(pSender) {
		pSender.setEnable(false);
		switch (pSender.getTag()){
			case TAG_BUTTON_LIB:
				if(ll.run.lib.isOpen()){
					gg.flow.location();
					ll.run.lib.close();
				} else {
					ll.run.lib.open();					
					ll.tip.arr.out();
				}
				pSender.setEnable(true);
		}
	}
})