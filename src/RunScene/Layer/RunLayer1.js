var RunLayer1 = cc.Layer.extend({
	arr:null,
	scene:null,
	clock:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 10);
		gg.main = this;
		this.init();
	},
	init:function () {
		this.callNext = cc.callFunc(function(){
			gg.flow.next();
		}, this);
		this.callKill = cc.callFunc(function(p){
			var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){
				p.removeFromParent(true);	
			}, this));
			p.runAction(seq);
		}, this); 

		this.callNext.retain();
		this.callKill.retain();
		//时钟
		this.clock = new Clock(this);
		// 物品库
		this.lib1 = new Lib(this);
		this.lib1.loadBg([TAG_LIB_ROD,TAG_LIB_AMPULLA,TAG_LIB_SPOON,TAG_LIB_THERMOMTER,TAG_LIB_WATER,
		                 TAG_LIB_NAOH,TAG_LIB_PIPE,TAG_LIB_ROCK,TAG_LIB_TESTTUBE,TAG_LIB_VOLUMETRIC]);
		//蒸发皿、钥匙、NaOH
		this.Surface = new Surface(this);

		this.Surface.setVisible(false);
		this.Surface.setPosition(540,384);
		//试管
//		this.Vitro = new Vitro(this);

//		this.Vitro.setVisible(false);
//		this.Vitro.setPosition(540,384);
	},
	loadInLib:function(obj, pos, tarPos,delay){
		obj.setPosition(pos);
		if(delay == null){
			delay = 1;
		}
		var ber = $.bezier(pos, tarPos, delay);
		var seq = cc.sequence(ber, this.callNext);
		obj.runAction(seq);
	},
	kill:function(obj){
		var fade = cc.fadeTo(0.5,0);
		var func = cc.callfunc(function(){
			obj.removeFromParent(true);
		},this);
		var seq = cc.sequence(fade,func);
		obj.runAction(seq)
	},

	callback:function (p){
		var func = cc.callFunc(this.actionDone, this);
		var action=gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_XIANGPISAI:

			break;

		}
	},
	actionDone:function(p){
		var func = cc.callFunc(this.actionDone, this);
		switch(p.getTag()){

		}
	},
	flowNext:function(){
		gg.flow.next();
	},
	onExit:function(){
		this._super();
		this.callNext.release();
		this.callKill.release();
	}
});