var TipLayer = cc.Layer.extend({
	scene:null,
	tip:null,
	time:null,
	score:null,
	flash:null,
	tip_frame:null,
	up_button:null,
	down_button:null,
	rotateAction:null,
	ctor:function (parent) {
		this._super();
		this.scene = parent;
		this.scene.addChild(this, 30);
		this.init();
	},
	init: function (){
		// 引导标记
		this.arr = new Arrow(this);
		this.addChild(this.arr, 100);

		this.tip = new Tip("", gg.width * 0.85);
		this.addChild(this.tip, 10);
		this.tip.setPosition(gg.width * 0.1, 10);

		this.flash = new Flash("", gg.width * 0.6);
		this.addChild(this.flash, 10);
		this.flash.setPosition(300, 620);

		this.tip_frame= new TipFrame(this,"#tip_frame.png");	
//		this.tip_frame = new WinFrame(this,"#tip_frame.png");

		this.tipItem = new ButtonScale(this,"#button/tip.png",this.eventMenuCallback);
		this.tipItem.setTag(TAG_TIP);
		this.tipItem.setPosition(40 + this.tipItem.width*0.5, gg.height - 550 - this.tipItem.height * 0.5 );

		this.backItem = new ButtonScale(this,"#button/back.png",this.eventMenuCallback);
		this.backItem.setTag(TAG_CLOSE);
		this.backItem.setPosition(40 + this.backItem.width * 0.5, gg.height - 40 - this.backItem.height * 0.5);

		this.time = new cc.LabelTTF("", gg.fontName, gg.fontSize);
		this.score = new cc.LabelTTF("分数：0", gg.fontName, gg.fontSize);
		this.time.setPosition(130, gg.height - 20);
		this.time.setAnchorPoint(0, 1);
		this.time.setColor(cc.color(255,255,255,0));
		this.score.setPosition(280, gg.height - 20);
		this.score.setAnchorPoint(0, 1);
		this.score.setColor(cc.color(255,255,255,0));	
		this.addChild(this.time, 12);
		this.addChild(this.score, 12);

		if(gg.teach_type == TAG_LEAD){
			this.score.setVisible(false);
		} else {
			this.tip.setVisible(false);
		}
		this.updateTime();
		this.schedule(this.updateTime, 1);

//		gg.tip_layer = this;
		this.tip_frame.loadSlide();
	},
	sayFlash:function(str){
		this.flash.doFlash(str);
	},
	updateTime:function(){
		var now = new Date();
		var ds = Math.round((now - gg.begin_time) / 1000);
		this.time.setString("计时：" + ds + "秒");
	},
	mdScore:function(score){
		if(!score){
			return;
		}		
		gg.score += score;
		if(gg.score<=0){
			gg.score=0;
		}
		this.updateScore();
	},
	updateScore:function(){
		this.score.setString("分数：" + gg.score);
	},
	over:function(){
		this.unschedule(this.updateTime);
		gg.end_time = new Date();
	},
	eventMenuCallback: function(pSender) {
		switch (pSender.getTag()){
		case TAG_TIP:
			if(this.tip_frame.isOpen()){
				cc.log("关闭提示");
				gg.synch_l = false;
				gg.logo_onclick = true;
				this.tip_frame.close();
			} else {
				cc.log("打开提示");
				gg.synch_l = true;
				gg.logo_onclick = false;
				this.tip_frame.refresh();
				this.tip_frame.open();

				// 定位帮助信息
				var step = gg.flow.getStep();
				this.tip_frame.local(step);
				ll.tip.mdScore(-3);
			}
			break;
		case TAG_CLOSE:
			$.runScene(new StartScene());
			break;
		default:
			break;
		}
	}
});