/**
 * 试管
 */
Vitro = Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_VITRO);
		this.init();
	},
		init : function(){
			this.setVisible(false);
			this.setCascadeOpacityEnabled(true);
			//试管1
			var vitro1 = new Button(this, 10, TAG_VITRO1, "#testTube.png",this.callback);
			vitro1.setPosition(cc.p(-180,-130));
			vitro1.setScale(0.8);
			var vitro11 = new Button(this, 10, TAG_VITRO11, "#red.png",this.callback);
			vitro11.setPosition(cc.p(-180,-130));
			vitro11.setScale(0.8);
			vitro11.setOpacity(0);
			var line11 = new Button(this, 11, TAG_LINE1, "#line1.png",this.callback);
			line11.setPosition(-180.5,-210);
			line11.setScale(0.8);
			line11.setOpacity(0);
			
			var vitro2 = new Button(this, 10, TAG_VITRO2, "#testTube.png",this.callback);
			vitro2.setPosition(cc.p(20,-130));
			vitro2.setScale(0.8);
			var vitro22 = new Button(this, 10, TAG_VITRO22, "#blue.png",this.callback);
			vitro22.setPosition(cc.p(20,-130));
			vitro22.setScale(0.8);
			vitro22.setOpacity(0);
			var line22 = new Button(this, 11, TAG_LINE2, "#line1.png",this.callback);
			line22.setPosition(19.5,-210);
			line22.setScale(0.8);
			line22.setOpacity(0);
			
			var vitro3 = new Button(this, 10, TAG_VITRO3, "#testTube.png",this.callback);
			vitro3.setPosition(cc.p(220,-130));
			vitro3.setScale(0.8);
			var vitro33 = new Button(this, 10, TAG_VITRO33, "#red.png",this.callback);
			vitro33.setPosition(cc.p(220,-130));
			vitro33.setScale(0.8);
			vitro33.setOpacity(0);
			var line33 = new Button(this, 11, TAG_LINE3, "#line1.png",this.callback);
			line33.setPosition(219.5,-210);
			line33.setScale(0.8);
			line33.setOpacity(0);
			
			var vitro4 = new Button(this, 10, TAG_VITRO4, "#testTube.png",this.callback);
			vitro4.setPosition(cc.p(420,-130));
			vitro4.setScale(0.8);
			var vitro44 = new Button(this, 10, TAG_VITRO44, "#blue.png",this.callback);
			vitro44.setPosition(cc.p(420,-130));
			vitro44.setScale(0.8);
			vitro44.setOpacity(0);
			var line44 = new Button(this, 11, TAG_LINE4, "#line1.png",this.callback);
			line44.setPosition(419.5,-210);
			line44.setScale(0.8);
			line44.setOpacity(0);
//			var lid1 = new Button(this, 10, TAG_LITMUS1_LID, "#lid.png",this.callback);
//			lid1.setPosition(-350, -10);
//			lid1.setScale(0.6);
			//二氧化氮瓶子
//			var ampulla2 = new Button(this, 10, TAG_LITMUS2, "#ampulla.png",this.callback);
//			ampulla2.setPosition(cc.p(780,150));
//			var lid2 = new Button(ampulla2, 10, TAG_LITMUS2_LID, "#lid.png",this.callback);
//			lid2.setPosition(90,203);
//			var ampulla2_gas = new Button(ampulla2, 10, TAG_LITMUS2_GAS, "#gasColor/color_6.png",this.callback);
//			ampulla2_gas.setPosition(cc.p(100,93));
//			ampulla2_gas.setScale(0.6);
		},
		gas_color:function(p){
			var animFrames = [];
			for (var i = 1; i < 7; i++) {
				var str = "gasColor/color_" + i + ".png";
				var frame = cc.spriteFrameCache.getSpriteFrame(str);
				animFrames.push(frame);
			}
			var animation = new cc.Animation(animFrames, 0.5,1);
			var action = cc.animate(animation);
			var seq=cc.sequence(action);
			p.runAction(seq);
		},	
		callback:function(p){
			switch(p.getTag()){
			case TAG_AMPULLA1_LID:
				var ber = cc.bezierBy(2,[cc.p(0,40),cc.p(-200,80),cc.p(-220,-195)]);
				var seq=cc.sequence(ber,cc.callFunc(function() {
					gg.flow.next();
				}, this));
				p.runAction(seq);
				break;
			case TAG_AMPULLA1:
				var move= cc.moveTo(1.5, cc.p(500,300));
				var rot= cc.rotateTo(1.5,90);
				var spa= cc.spawn(move,rot);
				var mo= cc.moveTo(1, cc.p(550,300));
				var seq= cc.sequence(cc.callFunc(function() {
					var ampulla2=this.getParent().getChildByTag(TAG_AMPULLA).getChildByTag(TAG_AMPULLA2);
					var mov = cc.moveTo(1.5, cc.p(756,303));
					var rot = cc.rotateTo(1.5, -90);
					var spa = cc.spawn(mov,rot);
					ampulla2.runAction(spa);
				}, this),spa,mo,cc.delayTime(0.5),cc.callFunc(function() {
					gg.flow.next();
				}, this));
				p.runAction(seq);
				break;
			case TAG_AMPULLA2_LID:
				var ampulla1=ll.run.ampulla.getChildByTag(TAG_AMPULLA1);
				var mov=cc.moveTo(1, cc.p(-55,203));
				var rot = cc.rotateTo(0.8, 90);
				var move=cc.moveTo(0.8, cc.p(-145,203));
				var spa=cc.spawn(move,rot);
				var seq=cc.sequence(mov,cc.callFunc(function() {			
					ampulla1.runAction(cc.moveTo(0.6,cc.p(558,300)));
				}, this),spa,cc.callFunc(function() {
					var show = new ShowTip("观察实验现象,右边的红色气体逐渐扩散到左边\n一段时间后，两边颜色均为淡红色",cc.p(420,170));
					var ampulla2_gas = ll.run.ampulla.getChildByTag(TAG_AMPULLA2).getChildByTag(TAG_AMPULLA2_GAS);
					ampulla2_gas.runAction(cc.fadeTo(4, 180));
					var gas = new cc.Sprite("#gasColor/color_1.png");
					ampulla1.addChild(gas,20);
					gas.setOpacity(80);
					gas.setScale(0.6);
					gas.setPosition(cc.p(100,90));
					gas.runAction(cc.sequence(cc.callFunc(function() {
						ll.run.ampulla.gas_color(gas);
					}, this),cc.delayTime(3),cc.fadeTo(2, 180)));
				}, this),cc.delayTime(8),cc.callFunc(function() {				
					gg.flow.next();
				}, this));
				p.runAction(seq);
				break;
			}
		}
	});
