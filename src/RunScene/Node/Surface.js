/**
 * NaOH
 */
Surface = Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_SURFACE);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//蒸发皿
		


		var surface = new Button(this, 12, TAG_SURFACE1, "#chao/surface.png",this.callback);
		surface.setPosition(cc.p(-80,-150));
		surface.setScale(0.8);
		


		var bottle3 = new Button(this, 12, TAG_BOTTLE3, "#chao/NaOH.png",this.callback);
		bottle3.setPosition(cc.p(120,-110));
		bottle3.setScale(0.8);
		var lid3 = new Button(this, 10, TAG_BOTTLE3_LID, "#chao/lid1.png",this.callback);
		lid3.setPosition(cc.p(120,-40));
		lid3.setScale(0.8);

		var spoon = new Button(this, 10, TAG_SPOON, "#chao/spoon.png",this.callback);
		spoon.setPosition(cc.p(320,-130));
		spoon.setScale(0.8);



	},
	lid2:function(p){
		var animFrames = [];
		for (var i = 1; i < 10; i++) {
			var str = "lid/lid1" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.05,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},

	openLib1:function(){
		var ber = cc.bezierBy(1.5,[cc.p(50,90),cc.p(90,-140),cc.p(90,-135)]);
		var rotate = cc.rotateTo(1.5, 180);
		var spawn = cc.spawn(ber, rotate);
		var seq = cc.sequence(spawn,cc.callFunc(function() {
			this.getChildByTag(TAG_BOTTLE3_LID).setRotation(180);
			
			gg.flow.next();
		},this));
		this.getChildByTag(TAG_BOTTLE3_LID).runAction(seq);
	},
	

	closeLib:function(){
		var ber = cc.bezierBy(1.5,[cc.p(-10,75),cc.p(-60,240),cc.p(-90,135)]);
		var rotate = cc.rotateTo(1.5, 0);
		var spawn = cc.spawn(ber, rotate);
		var seq = cc.sequence(spawn,cc.callFunc(function() {
			this.getChildByTag(TAG_BOTTLE3_LID).setRotation(0);
	    	//	gg.flow.next();
		},this),cc.callFunc(function() {
			ll.run.clock.doing();
		}, this),cc.delayTime(4),cc.callFunc(function() {
			ll.run.clock.stop();
			ll.run.clock.time.setVisible(true);			
		}, this),cc.delayTime(1),cc.callFunc(function(){
			ll.run.clock.time.setVisible(false);
			ll.run.clock.setVisible(false);
		},this),cc.callFunc(function(){
			this.getChildByTag(TAG_SURFACE1).setSpriteFrame("chao/surface2.png");
		},this),
		cc.callFunc(function() {
			ll.run.clock.doing();
		}, this),cc.delayTime(4),cc.callFunc(function() {
			ll.run.clock.stop();
			ll.run.clock.time.setVisible(true);			
		}, this),cc.delayTime(1),cc.callFunc(function(){
			ll.run.clock.time.setVisible(false);
			ll.run.clock.setVisible(false);
		},this),cc.callFunc(function(){
			this.getChildByTag(TAG_SURFACE1).setSpriteFrame("chao/surface3.png");
		},this),cc.delayTime(3),
		cc.callFunc(function(){
			gg.flow.next();
		},this));

		
		this.getChildByTag(TAG_BOTTLE3_LID).runAction(seq);

	
	},
	callback:function(p){
		var action = gg.flow.flow.action;
	
		var lid33=ll.run.Surface.getChildByTag(TAG_BOTTLE3_LID);
		var spoon=ll.run.Surface.getChildByTag(TAG_SPOON);
		switch(p.getTag()){
		case  TAG_SPOON:

			var ber = cc.bezierBy(2.5,[cc.p(0,350),cc.p(-195,280),cc.p(-195,65)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(0,200),cc.p(-100,320),cc.p(-155,-55)]);
			var seq=cc.sequence(cc.rotateTo(0.25,-25),ber,cc.delayTime(1),cc.callFunc(function() {
				this.getChildByTag(TAG_SPOON).setSpriteFrame("chao/spoon2.png");}, this),cc.rotateTo(0.25,0),ber1,cc.rotateTo(0.5,-15),cc.callFunc(function() 
			{
				this.getChildByTag(TAG_SPOON).setSpriteFrame("chao/spoon.png");
				this.getChildByTag(TAG_SURFACE1).setSpriteFrame("chao/surface1.png");
				this.getChildByTag(TAG_SPOON).runAction(cc.fadeOut(0.5));
			}, this),
			cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

//			lid22.runAction( cc.sequence( cc.callFunc(function(){
//				ll.run.Litmus.lid2(lid22);
//			},this),cc.delayTime(3.5),cc.callFunc(function(){
//				var fade1=cc.fadeTo(0.5, 255);
//				var move=cc.moveTo(1,cc.p(-180.5,-160));
//				line.runAction(cc.sequence(fade1,move));
//				ll.run.Litmus.lid3(lid22);
//			},this)));


			break;

		case  TAG_BOTTLE3_LID:
			if(action==ACTION_DO1)
				{
			this.openLib1();
				};
			
			if(action==ACTION_DO2)
			{
				this.closeLib();
			}

			break;
		case  TAG_BOTTLE4_LID:

			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,65)]);
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

			lid44.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid44);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(219.5,-160));
				line3.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid44);
			},this)));




			break;
		case  TAG_BOTTLE5_LID:

			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,55)]);
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

			lid55.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid55);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(419.5,-160));
				line4.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid55);
			},this)));



			break;
		case TAG_LITMUS1_LID:
			var lid1=ll.run.Litmus.getChildByTag(TAG_LITMUS1_LID);
			var vitro11=ll.run.Vitro.getChildByTag(TAG_VITRO11);
			var vitro22=ll.run.Vitro.getChildByTag(TAG_VITRO22);
			var vitro33=ll.run.Vitro.getChildByTag(TAG_VITRO33);
			var vitro44=ll.run.Vitro.getChildByTag(TAG_VITRO44);
			if(action==ACTION_DO1){
				var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(50,80),cc.p(100,-55)]);
				var ber1= cc.bezierBy(2.5,[cc.p(0,255),cc.p(-120,220),cc.p(-100,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));

				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(2.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro11.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));

				p.runAction(seq);
			}
			if(action==ACTION_DO2){
				var ber = cc.bezierBy(3.5,[cc.p(0,260),cc.p(300,260),cc.p(300,-55)]);
				var ber1= cc.bezierBy(3.5,[cc.p(0,280),cc.p(-330,280),cc.p(-300,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));

				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(3.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro22.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));

				p.runAction(seq);
			}
			if(action==ACTION_DO3){
				var ber = cc.bezierBy(4,[cc.p(0,260),cc.p(500,260),cc.p(500,-55)]);
				var ber1= cc.bezierBy(4,[cc.p(0,280),cc.p(-580,380),cc.p(-500,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));

				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(4),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro33.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));
				p.runAction(seq);
			}
			if(action==ACTION_DO4){
				var ber = cc.bezierBy(4.5,[cc.p(0,260),cc.p(700,260),cc.p(700,-55)]);
				var ber1= cc.bezierBy(4.5,[cc.p(0,280),cc.p(-780,380),cc.p(-700,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));	

				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(4.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro44.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));
				p.runAction(seq);
			}
			break;

		}
	}
});
