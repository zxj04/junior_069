/**
 * 石蕊
 */
Litmus = Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_LITMUS);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//石蕊
		var litmus1 = new Button(this, 12, TAG_LITMUS1, "#ampulla.png",this.callback);
		litmus1.setPosition(cc.p(-280,80));
		litmus1.setScale(0.8);
		var lid1 = new Button(this, 11, TAG_LITMUS1_LID, "#lid10.png",this.callback);
		lid1.setPosition(-280, 120);
		lid1.setScale(0.6);
		
		
		
		var bottle2 = new Button(this, 12, TAG_BOTTLE2, "#hcl2.png",this.callback);
		bottle2.setPosition(cc.p(-80,80));
		bottle2.setScale(0.8);
		var lid2 = new Button(this, 10, TAG_BOTTLE2_LID, "#lid10.png",this.callback);
		lid2.setPosition(cc.p(-80,120));
		lid2.setScale(0.6);
		var line2 = new Button(this, 11, TAG_LINE1_LID, "#line.png",this.callback);
		line2.setPosition(-80,80);
		line2.setScale(0.8);

		
		var bottle3 = new Button(this, 12, TAG_BOTTLE3, "#NaOH2.png",this.callback);
		bottle3.setPosition(cc.p(120,80));
		bottle3.setScale(0.8);
		var lid3 = new Button(this, 10, TAG_BOTTLE3_LID, "#lid10.png",this.callback);
		lid3.setPosition(cc.p(120,120));
		lid3.setScale(0.6);
		var line3 = new Button(this, 11, TAG_LINE2_LID, "#line.png",this.callback);
		line3.setPosition(120,80);
		line3.setScale(0.8);
		
		var bottle4 = new Button(this, 12, TAG_BOTTLE4, "#h2so42.png",this.callback);
		bottle4.setPosition(cc.p(320,80));
		bottle4.setScale(0.8);
		var lid4 = new Button(this, 10, TAG_BOTTLE4_LID, "#lid10.png",this.callback);
		lid4.setPosition(320,120);
		lid4.setScale(0.6);
		var line4 = new Button(this, 11, TAG_LINE3_LID, "#line.png",this.callback);
		line4.setPosition(320,80);
		line4.setScale(0.8);
		
	
	
		var bottle5 = new Button(this, 12, TAG_BOTTLE5, "#CaoH22.png",this.callback);
		bottle5.setPosition(cc.p(520,80));
		bottle5.setScale(0.8);
		var lid5 = new Button(this, 10, TAG_BOTTLE5_LID, "#lid10.png",this.callback);
		lid5.setPosition(520,120);
		lid5.setScale(0.6);
		var line5 = new Button(this, 11, TAG_LINE4_LID, "#line.png",this.callback);
		line5.setPosition(520,80);
		line5.setScale(0.8);
		
		
	},
	lid2:function(p){
		var animFrames = [];
		for (var i = 1; i < 10; i++) {
			var str = "lid/lid1" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.05,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},
	lid3:function(p){
		var animFrames = [];
		for (var i = 9; i > -1; i--) {
			var str = "lid/lid1" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.05,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},
	lid4:function(p){
		var animFrames = [];
		for (var i = 1; i < 10; i++) {
			var str = "lid1/lid1" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.05,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},
	lid5:function(p){
		var animFrames = [];
		for (var i = 9; i > -1; i--) {
			var str = "lid1/lid1" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.10,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},

	genPoint:function(p,count){
		count --;
		if(count <= 0){
			return;
		}
		var wp = new cc.Sprite("#shuidi5.png")
		wp.setPosition(20, 0);
		wp.setScale(1.5);
		p.addChild(wp);
		var move = cc.moveBy(0.25, cc.p(0, -65));
		var func = cc.callFunc(function(){
			wp.removeFromParent(true);
			this.genPoint(p,count);
		}, this);
		var seq = cc.sequence(move, func);
		wp.runAction(seq);
	},
	genPoint1:function(p,count){
		count --;
		if(count <= 0){
			return;
		}
		var wp1 = new cc.Sprite("#shuidi3.png")
		wp1.setPosition(40, 0);
		wp1.setScale(3);
		p.addChild(wp1);
		var move = cc.moveBy(0.25, cc.p(0, -65));
		var func = cc.callFunc(function(){
			wp1.removeFromParent(true);
			this.genPoint1(p,count);
		}, this);
		var seq = cc.sequence(move, func);
		wp1.runAction(seq);
	},
	
	callback:function(p){
		var action = gg.flow.flow.action;
		var lid22=ll.run.Litmus.getChildByTag(TAG_BOTTLE2_LID);
		var lid33=ll.run.Litmus.getChildByTag(TAG_BOTTLE3_LID);
		var lid44=ll.run.Litmus.getChildByTag(TAG_BOTTLE4_LID);
		var lid55=ll.run.Litmus.getChildByTag(TAG_BOTTLE5_LID);
		var vitro1=ll.run.Vitro.getChildByTag(TAG_VITRO1);
		var line=ll.run.Vitro.getChildByTag(TAG_LINE1);
		
		var vitro2=ll.run.Vitro.getChildByTag(TAG_VITRO2);
		var line2=ll.run.Vitro.getChildByTag(TAG_LINE2);
		var vitro3=ll.run.Vitro.getChildByTag(TAG_VITRO3);
		var line3=ll.run.Vitro.getChildByTag(TAG_LINE3);
		var vitro4=ll.run.Vitro.getChildByTag(TAG_VITRO4);
		var line4=ll.run.Vitro.getChildByTag(TAG_LINE4);
		switch(p.getTag()){
		case  TAG_BOTTLE2_LID:
			
			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,55)]);
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);
				
			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);
			
			lid22.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid22);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(-180.5,-160));
				line.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid22);
			},this)));
		   
			
			break;
		
		case  TAG_BOTTLE3_LID:

			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,55)]);
			
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

			lid33.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid33);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(19.5,-160));
				line2.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid33);
			},this)));
			

			break;
		case  TAG_BOTTLE4_LID:

			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,55)]);
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

			lid44.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid44);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(219.5,-160));
				line3.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid44);
			},this)));


			

			break;
		case  TAG_BOTTLE5_LID:

			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(-150,80),cc.p(-100,-55)]);
			var ber1 = cc.bezierBy(2.5,[cc.p(-0,200),cc.p(100,320),cc.p(100,55)]);
			var seq=cc.sequence(cc.delayTime(1),ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);

			lid55.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid2(lid55);
			},this),cc.delayTime(3.5),cc.callFunc(function(){
				var fade1=cc.fadeTo(0.5, 255);
				var move=cc.moveTo(1,cc.p(419.5,-160));
				line4.runAction(cc.sequence(fade1,move));
				ll.run.Litmus.lid3(lid55);
			},this)));



			break;
		case TAG_LITMUS1_LID:
			var lid1=ll.run.Litmus.getChildByTag(TAG_LITMUS1_LID);
			var vitro11=ll.run.Vitro.getChildByTag(TAG_VITRO11);
			var vitro22=ll.run.Vitro.getChildByTag(TAG_VITRO22);
			var vitro33=ll.run.Vitro.getChildByTag(TAG_VITRO33);
			var vitro44=ll.run.Vitro.getChildByTag(TAG_VITRO44);
			if(action==ACTION_DO1){
			var ber = cc.bezierBy(2.5,[cc.p(0,250),cc.p(50,80),cc.p(100,-55)]);
			var ber1= cc.bezierBy(2.5,[cc.p(0,255),cc.p(-120,220),cc.p(-100,55)]);
			var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
				this.genPoint1(p, 4);

			}, this)),ber1,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			
			lid1.runAction( cc.sequence( cc.callFunc(function(){
				ll.run.Litmus.lid4(lid1);
			},this),cc.delayTime(2.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro11.runAction(cc.sequence(fade1));
				ll.run.Litmus.lid5(lid1);
			},this)));
			
			p.runAction(seq);
			}
			if(action==ACTION_DO2){
				var ber = cc.bezierBy(3.5,[cc.p(0,260),cc.p(300,260),cc.p(300,-55)]);
				var ber1= cc.bezierBy(3.5,[cc.p(0,280),cc.p(-330,280),cc.p(-300,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));
				
				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(3.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro22.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));
				
				p.runAction(seq);
			}
			if(action==ACTION_DO3){
				var ber = cc.bezierBy(4,[cc.p(0,260),cc.p(500,260),cc.p(500,-55)]);
				var ber1= cc.bezierBy(4,[cc.p(0,280),cc.p(-580,380),cc.p(-500,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));
				
				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(4),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro33.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));
				p.runAction(seq);
			}
			if(action==ACTION_DO4){
				var ber = cc.bezierBy(4.5,[cc.p(0,260),cc.p(700,260),cc.p(700,-55)]);
				var ber1= cc.bezierBy(4.5,[cc.p(0,280),cc.p(-780,380),cc.p(-700,55)]);
				var seq=cc.sequence(ber,cc.spawn(cc.delayTime(2),cc.callFunc(function() {
					this.genPoint1(p, 4);

				}, this)),ber1,cc.callFunc(function() {
					gg.flow.next();
				}, this));	
				
				lid1.runAction( cc.sequence( cc.callFunc(function(){
					ll.run.Litmus.lid4(lid1);
				},this),cc.delayTime(4.5),cc.callFunc(function(){
					var fade1=cc.fadeTo(1, 255);
					//var move=cc.moveTo(1,cc.p(-180.5,-160));
					vitro44.runAction(cc.sequence(fade1));
					ll.run.Litmus.lid5(lid1);
				},this)));
				p.runAction(seq);
			}
			break;

		}
	}
});
