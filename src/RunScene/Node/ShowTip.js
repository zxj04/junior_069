/**
 * 文字自带边框的实体
 */
ShowTip = cc.Node.extend({
	ctor:function(name,pos,next){
		this._super();
		ll.run.addChild(this, 100, TAG_SHOW);
		this.setCascadeOpacityEnabled(true);
		this.init(name,pos,next);
	},
	init:function(name,pos,next){
		var content = new cc.LabelTTF(name, gg.fontName, gg.fontSize);
		var mr = 10;
		var rect = content.getBoundingBoxToWorld();
		var bg = new cc.Scale9Sprite(res_start.show_tip);
		bg.width = rect.width + mr * 5;
		bg.height = rect.height + mr * 5;
		bg.setPosition(pos );
		//rect.width * 0.5, rect.height * 0.5
		this.addChild(bg);
		bg.addChild(content,2);
		content.setColor(cc.color(255, 255, 255));
		content.setPosition(cc.p(bg.width*0.5,bg.height*0.5));

		if(next == null){
			next=false;
		}
		if(next){
			this.scheduleOnce(this.kill, 1);	
		}
	},
	kill:function(){

		var seq = cc.sequence(cc.fadeOut(0.5),cc.callFunc(function(){

			this.removeFromParent(true);	
	
		}, this))
		this.runAction(seq);
	}
})