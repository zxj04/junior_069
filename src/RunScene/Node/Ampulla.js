/**
 * 玻璃瓶
 */
Ampulla = Widget.extend({
	ctor:function(parent){
		this._super();
		parent.addChild(this,30);
		this.setTag(TAG_AMPULLA);
		this.init();
	},
	init : function(){
		this.setVisible(false);
		this.setCascadeOpacityEnabled(true);
		//空气瓶子
		var ampulla1 = new Button(this, 10, TAG_AMPULLA1, "#ampulla.png",this.callback);
		ampulla1.setPosition(cc.p(500,150));
		var lid1 = new Button(this, 10, TAG_AMPULLA1_LID, "#lid.png",this.callback);
		lid1.setPosition(490, 253);
		//二氧化氮瓶子
		var ampulla2 = new Button(this, 10, TAG_AMPULLA2, "#ampulla.png",this.callback);
		ampulla2.setPosition(cc.p(780,150));
		var lid2 = new Button(ampulla2, 10, TAG_AMPULLA2_LID, "#lid.png",this.callback);
		lid2.setPosition(90,203);
		var ampulla2_gas = new Button(ampulla2, 10, TAG_AMPULLA2_GAS, "#gasColor/color_6.png",this.callback);
		ampulla2_gas.setPosition(cc.p(100,93));
		ampulla2_gas.setScale(0.6);
	},
	gas_color:function(p){
		var animFrames = [];
		for (var i = 1; i < 7; i++) {
			var str = "gasColor/color_" + i + ".png";
			var frame = cc.spriteFrameCache.getSpriteFrame(str);
			animFrames.push(frame);
		}
		var animation = new cc.Animation(animFrames, 0.5,1);
		var action = cc.animate(animation);
		var seq=cc.sequence(action);
		p.runAction(seq);
	},	
	callback:function(p){
		switch(p.getTag()){
		case TAG_AMPULLA1_LID:
			var ber = cc.bezierBy(2,[cc.p(0,40),cc.p(-200,80),cc.p(-220,-195)]);
			var seq=cc.sequence(ber,cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);
			break;
		case TAG_AMPULLA1:
			var move= cc.moveTo(1.5, cc.p(500,300));
			var rot= cc.rotateTo(1.5,90);
			var spa= cc.spawn(move,rot);
			var mo= cc.moveTo(1, cc.p(550,300));
			var seq= cc.sequence(cc.callFunc(function() {
				var ampulla2=this.getParent().getChildByTag(TAG_AMPULLA).getChildByTag(TAG_AMPULLA2);
				var mov = cc.moveTo(1.5, cc.p(756,303));
				var rot = cc.rotateTo(1.5, -90);
				var spa = cc.spawn(mov,rot);
				ampulla2.runAction(spa);
			}, this),spa,mo,cc.delayTime(0.5),cc.callFunc(function() {
				gg.flow.next();
			}, this));
			p.runAction(seq);
			break;
		case TAG_AMPULLA2_LID:
			var ampulla1=ll.run.ampulla.getChildByTag(TAG_AMPULLA1);
			var mov=cc.moveTo(1, cc.p(-55,203));
			var rot = cc.rotateTo(0.8, 90);
			var move=cc.moveTo(0.8, cc.p(-145,203));
			var spa=cc.spawn(move,rot);
			var seq=cc.sequence(mov,cc.callFunc(function() {			
				ampulla1.runAction(cc.moveTo(0.6,cc.p(558,300)));
			}, this),spa,cc.callFunc(function() {
				var show = new ShowTip("观察实验现象,右边的红色气体逐渐扩散到左边\n一段时间后，两边颜色均为淡红色",cc.p(420,170));
				var ampulla2_gas = ll.run.ampulla.getChildByTag(TAG_AMPULLA2).getChildByTag(TAG_AMPULLA2_GAS);
				ampulla2_gas.runAction(cc.fadeTo(4, 180));
				var gas = new cc.Sprite("#gasColor/color_1.png");
				ampulla1.addChild(gas,20);
				gas.setOpacity(80);
				gas.setScale(0.6);
				gas.setPosition(cc.p(100,90));
				gas.runAction(cc.sequence(cc.callFunc(function() {
					ll.run.ampulla.gas_color(gas);
				}, this),cc.delayTime(3),cc.fadeTo(2, 180)));
			}, this),cc.delayTime(8),cc.callFunc(function() {				
				gg.flow.next();
			}, this));
			p.runAction(seq);
			break;
		}
	}
});
