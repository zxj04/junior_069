/**
 * 药品库
 */
Lib1 = cc.Sprite.extend({
	libArr: [],
	openFlag:true,
	doing:false,
	doright:true,
	rm : 5,
	error:true,
	ctor:function(p){
		this._super("#libshow.png");
		p.addChild(this,300);
		this.setTag(TAG_LIB1);
		this.setCascadeOpacityEnabled(true);
//		var libbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
//		p.addChild(libbg,10, TAG_LIBBG);
//		libbg.setOpacity(0);
		this.loadButton();
	},
	loadButton:function(){
		this.cancel = new ButtonScale(this,"#button/cancel.png",this.action);
		this.sure = new ButtonScale(this,"#button/sure.png",this.action);
		this.cancel.setPosition(cc.p(this.width - 84 - this.sure.width*0.5 - 56 - this.cancel.width,  35 + this.cancel.height*0.5));
		this.sure.setPosition(cc.p(this.width - 84 - this.sure.width*0.5, 35 + this.sure.height*0.5 ));
		this.cancel.setTag(TAG_CANCEL);
		this.sure.setTag(TAG_SURE);
	},
	loadBg:function(tags){
		this.setPosition(cc.p(115 + this.width*0.5, 144 + this.height*0.5))
		this.bgArr = [];
		this.checkArr = [];
		this.sureArr = [];
		var first = true;
		var firstPos = cc.p( 20-5 , this.height - 26 + 5);
		var prev = null;
		var num = 0;
		var j =1;
		for(var i in tags){
			var bg = new Angel(this,"#uncheck.png", this.callback);
			bg.setTag(TAG_LIB_BG+j);
			j++;
			bg.setAnchorPoint(0,1);
			this.bgArr.push(bg);
			var rel = this.getLibRel(tags[i]);
			var lib = new LibButton(bg,4, rel.tag, rel.img, this.callback,this);
			bg.setCascadeOpacityEnabled(true);
			lib.setCascadeOpacityEnabled(true);
			lib.setPosition(cc.p(bg.width*0.5,bg.height*0.5+10));
			if(first){
				first = false;
				bg.setPosition(firstPos);

			} else {
				if(i==0||i==1||i==4||i==6||i==7||i==8||i==9){
					this.checkArr.push(bg);//错误的仪器	
				}else {
					this.sureArr.push(bg);//正确的仪器	
				}	

				if(num == 5){
					bg.down(prev, this.rm);
				}else if(num >5){
					bg.left(prev, this.rm);
				}else{
					bg.right(prev, this.rm);
				}				
			}
			prev = bg;
			num++;
		}
	},
	getLibRel:function(tag){
		for(var i in libRelArr){
			if(libRelArr[i].tag == tag){
				return libRelArr[i];
			}
		}
	},
	callback:function(p){
		var pos = this.getPosition(); 
		var action = gg.flow.flow.action;
		switch(p.getTag()){
		case TAG_LIB_AMPULLA:
			cc.log("就是这个！！")

			break;	
//			case TAG_LIB_LITMUS:
//			cc.log("就是这个！！")

//			break;	

//			case TAG_LIB_H2SO4:
//			cc.log("就是这个！！")

//			break;	
//			case TAG_LIB_HCL:
//			cc.log("就是这个！！")

//			break;	
//			case	TAG_LIB_CAOH2:
//			cc.log("就是这个！！")

//			break;	
//			case	TAG_LIB_NAOH:
//			cc.log("就是这个！！")

//			break;	

//			case TAG_LIB_TESTTUBE:
//			cc.log("就是这个！！")

//			break;	
		default:
			break;	
		}
		var bg = p.getParent();
		if(bg.getTag()!==TAG_LIB){				
			if(!bg.check){
				bg.setSpriteFrame("check.png");
				bg.check = true;
				p.check = true;
			}else{
				bg.setSpriteFrame("uncheck.png");
				bg.check = false;
				p.check = false;
			}			
		}else{
			if(!p.check){
				p.setSpriteFrame("check.png");
				p.check = true;
			}else{
				p.setSpriteFrame("uncheck.png");
				p.check = false;
			}	
		}

	},
	action:function(p){
		var max = 1;
		switch(p.getTag()){
		case TAG_SURE:
			if(this.doright){
				for(var i  in this.checkArr){					 
					if(this.checkArr[i].check  ){
						this.showTip = new ShowTip("仪器选择错误！",cc.p(gg.width*0.4,220),true);
						ll.tip.mdScore(-3);
						this.error=true;
						return;
					}
				}
				for (var i  in this.sureArr){
					if(!this.sureArr[i].check ){
						//if(ll.run.getChildByTag(TAG_SHOW)==null){
						this.showTip = new ShowTip("选择仪器缺少！",cc.p(gg.width*0.4,220),true);	
						ll.tip.mdScore(-3);
						this.error=true;
						return;
						//	}	
					}
					if(max==this.sureArr.length){
						gg.flow.next();
						this.doright=false;		
						this.close();
						var surface = ll.run.Surface;
						surface.setVisible(true);


						ll.tip.mdScore(10);
						this.error=false;
						cc.log("max:"+max);
					}

					max=max+1;
				}


			}

			else{
				this.showTip = new ShowTip("仪器已选择，请点击关闭并返回实验！",cc.p(gg.width*0.4-50,200),true);
			}
			this.error=false;
			break;

		case TAG_CANCEL:
			this.close();
			break;
		}	
	},
	isOpen:function(){
		return this.openFlag; 
	},
	open:function(){
		if(this.openFlag || this.doing){
			return;
		}
		ll.run.Surface.setVisible(false);


		var fadein = cc.fadeIn(0.4);
		var func = cc.callFunc(function(){
			this.openFlag = true;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 显示箭头
					gg.flow.location();
				}
			}
		}, this);
		var seq = cc.sequence(fadein,func);
		this.runAction(seq);
	},
	close:function(){	
		if(!this.openFlag || this.doing){
			return;
		}	
		if(!this.error){
			ll.run.Surface.setVisible(true);

		}

		this.doing = true;
		var fadeout = cc.fadeOut(0.4);
		var func = cc.callFunc(function(){
			this.openFlag = false;
			this.doing = false;
			var tag = gg.flow.flow.tag;
			if(tag instanceof Array){
				if(TAG_LIB_MIN < tag[1]){
					// 隐藏箭头
					//ll.tip.arr.out();
					//ll.tip.arr.setPosition(gg.width-45,455);
					ll.tip.arr.pos(ll.tool.getChildByTag(TAG_BUTTON_LIB));
				}
			}
		}, this);
		var seq = cc.sequence(fadeout,func);
		this.runAction(seq);
	}
});


TAG_LIB_AMPULLA = 30001;
TAG_LIB_LITMUS=30002;
TAG_LIB_H2SO4=30003;
TAG_LIB_THERMOMTER=30004;
TAG_LIB_CAOH2=30005;
TAG_LIB_NAOH=30006;
TAG_LIB_PIPE=30007;
TAG_LIB_ROCK=30008;
TAG_LIB_TESTTUBE=30009;
TAG_LIB_VOLUMETRIC=30010;
TAG_LIB_SURFACE=30011;

TAG_LIB_SPOON=30012;
TAG_LIB_ROD = 30013;
TAG_LIB_WATER = 30014;

TAG_LIB_BG=30050;
TAG_LIB_BG1=30051;
TAG_LIB_BG2=30052;
TAG_LIB_BG3=30053;
TAG_LIB_BG4=30054;
TAG_LIB_BG5=30055;
TAG_LIB_BG6=30056;
TAG_LIB_BG7=30057;
TAG_LIB_BG8=30058;
TAG_LIB_BG9=30059;
TAG_LIB_BG10=30060;

TAG_CANCEL=30081;
TAG_SURE=30082;

libRelArr = [
             {tag:TAG_LIB_ROD, name:"玻璃棒",img:"#shao/rod.png"},  
            
             {tag:TAG_LIB_AMPULLA, name:"玻璃瓶",img:"#beaker.png"},

             {tag:TAG_LIB_SPOON,name:"钥匙",img:"#chao/spoon.png"},
             {tag:TAG_LIB_THERMOMTER,name:"温度计",img:"#shao/thermomter.png"},
             {tag:TAG_LIB_WATER,name:"蒸馏水",img:"#shao/water.png"},
             {tag:TAG_LIB_NAOH, name:"氢氧化钠",img:"#chao/NaOH1.png"},
             {tag:TAG_LIB_PIPE,name:"冷凝管",img:"#pipe.png"},
             {tag:TAG_LIB_ROCK,name:"沸石",img:"#rock.png"},
             {tag:TAG_LIB_TESTTUBE,name:"试管",img:"#testTube.png"},
             {tag:TAG_LIB_VOLUMETRIC,name:"烧瓶",img:"#volumetric.png"},
             ];
