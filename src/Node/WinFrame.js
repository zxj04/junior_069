WinFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	cell:null,
	ctor:function (parent,fileName) {
		this._super();
		parent.addChild(this,10000);
		this.setPosition(0, gg.height);
		this.init(fileName);
		var tipbg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		parent.addChild(tipbg,10, TAG_TIPSTARTBG);
		tipbg.setOpacity(0);
	},
	init:function (fileName){
		var frame = new cc.Sprite(fileName);
		frame.setPosition(gg.c_p);
		this.addChild(frame,2);

		var close = new ButtonScale(this,"#tip_frame_close.png",this.callback);
		close.setPosition(1135, 655);
		close.setLocalZOrder(3);
	},
	open:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 200));		
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		this.stopAllActions();
		var tipbg = this.getParent().getChildByTag(TAG_TIPSTARTBG);
		tipbg.runAction(cc.fadeTo(0.4, 0));
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	buttonSetEnable:function(next){
		var leadButton = this.getParent().getChildByTag(TAG_LEAD);
		var realButton = this.getParent().getChildByTag(TAG_REAL);
		var purposeButton = this.getParent().getChildByTag(TAG_PURPOSE);
		var principleButton = this.getParent().getChildByTag(TAG_PRINCIPLE);
		var gameButton = this.getParent().getChildByTag(TAG_GAME);
		leadButton.setEnable(next);
		realButton.setEnable(next);
		purposeButton.setEnable(next);
		principleButton.setEnable(next);
		gameButton.setEnable(next);
	},
	isOpen:function (){
		return this.open_flag;
	},
	callback:function(){
		if(this.isOpen()){
			cc.log("关闭提示");
			gg.synch_l = false;
			this.close();
			this.buttonSetEnable(true);
		}
	}
});