Tip = cc.LabelTTF.extend({
	fSize:0,
	d_width:null,
	ctor:function (text, width, fontName,fontSize) {
		this.d_width = width;
		if(fontName == null){
			fontName = gg.fontName;
		}
		if(fontSize == null){
			this.fSize = gg.fontSize;
		} else {
			this.fSize = fontSize;
		}
		text = $.format(text, this.d_width, this.fSize);
		this._super(text, fontName, this.fSize);
		this.setAnchorPoint(0, 0);
// this.setColor(cc.color(0,0,0,0));
	},
	doTip:/**
			 * 更改提示
			 * 
			 * @param text
			 */
	function(text){
		text = $.format(text, this.d_width, this.fSize);
		this.setString(text);
	}
});

TipFrame = cc.Layer.extend({
	cur : 0,
	tip : [],
	lineMarign:10,
	posX: 0,
	posY: 0,
	open_flag: false,
	tipLayer:null,
	cell:null,
	ctor:function (parent,fileName) {
		this._super();
		parent.addChild(this,10000);
		this.setPosition(0, gg.height);
		this.init(fileName);
		var tipRunBg = new cc.LayerColor(cc.color(0, 0, 0, 200),1280,768);
		parent.addChild(tipRunBg,10, TAG_TIPRUNBG);
		tipRunBg.setOpacity(0);
	},
	init:function (fileName){
		var frame = new cc.Sprite(fileName);
		frame.setPosition(gg.c_p);
		this.addChild(frame,2);

		var close = new ButtonScale(this,"#tip_frame_close.png",this.callback);
		close.setPosition(1135, 655);
		close.setLocalZOrder(3);
		
		//增加遮罩
		var stencil = new cc.DrawNode()
		stencil.drawRect(cc.p(270,130),cc.p(1030,570),cc.color(248,232,176),0,cc.color(248,232,176));
		var clipping = new cc.ClippingNode(stencil);
//		clipping.setInverted(true); 
		this.addChild(clipping, 20,TAG_CLIPPING);
		clipping.setStencil(stencil);
		clipping.stencil = stencil;
		
		this.node = new cc.Node();
		clipping.addChild(this.node);
		this.cur = 0;
		this.posX = gg.width*0.3;
		this.posY = gg.height-200;
		for(var i = 0;i< teach_flow.length; i++){
			var pre = i - -1 + ".";
			var text = $.format(pre + teach_flow[i].tip, 0.5 * gg.width, 20);
			var tip = new cc.LabelTTF(text, gg.fontName, 20);
			tip.setColor(cc.color(0, 0, 0));
			tip.setAnchorPoint(0, 0);
			this.posY = this.posY - tip.height - this.lineMarign;
			tip.setPosition(this.posX, this.posY);
			this.tip[i] = tip;
			this.node.addChild(tip);
			if(this.cell == null){
				this.cell = tip.height + this.lineMarign;
			}
		}
	},
	open:function (){
		this.stopAllActions();
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG);
		tipRunBg.runAction(cc.fadeTo(0.4, 200));		
		var move = cc.moveTo(0.3,cc.p(0, 0));
		var move2 = cc.moveTo(0.1,cc.p(0, 50));
		var move3 = cc.moveTo(0.1,cc.p(0, 0));
		var sequence = cc.sequence(move, move2, move3);
		this.runAction(sequence);
		AngelListener.setEnable(false);
		this.open_flag = true;
	},
	close:function (){
		this.stopAllActions();
		var tipRunBg = this.getParent().getChildByTag(TAG_TIPRUNBG);
		tipRunBg.runAction(cc.fadeTo(0.4, 0));
		var move = cc.moveTo(0.3,cc.p(0, gg.height));
		var sequence = cc.sequence(move);
		this.runAction(sequence);
		AngelListener.setEnable(true);
		this.open_flag = false;
	},
	isOpen:function (){
		return this.open_flag;
	},
	up:function (){
		if(this.cur <= 0){
			return;
		}
		var curTip = this.tip[--this.cur];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, - curTip.height - this.lineMarign)));
	},
	down:function (){
		if(this.cur >= this.tip.length - 1){
			return;
		}
		var curTip = this.tip[this.cur++];
		this.node.runAction(cc.moveBy(0.2,cc.p(0, curTip.height + this.lineMarign)));
	},
	local:function(step){
		var last = this.cur;
		this.cur = step - 1;
		if(this.cur < 0){
			this.cur = 0;
		}
		var result = 0;
		if(this.cur == last){
			return;
		} else if(this.cur > last){
			for(var i = last; i < this.cur; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
		} else {
			for(var i = this.cur; i < last; i ++){
				result += this.tip[i].height;
				result += this.lineMarign;
			}
			result = -result;
		}
		var pos = this.node.getPosition();
		this.node.setPosition(pos.x,pos.y + result);
	},
	refresh:function (){
		for(var i=0;i<this.tip.length;i++){
			if(teach_flow[i].cur){
				this.tip[i].setColor(cc.color(137,64,0));	
			} else {
				this.tip[i].setColor(cc.color(205,157,100));
			}
		}	
	},
	callback:function(){
		if(this.isOpen()){
			cc.log("关闭提示");
			gg.synch_l = false;
			this.close();
		}
	},
	loadSlide:/**
				 * 滑动
				 */
		function(){
		var listener_touch = cc.EventListener.create({
			event: cc.EventListener.TOUCH_ONE_BY_ONE,
			swallowTouches: false,
			hover: false,
			onTouchBegan:this.onTouchBegan,
			onTouchMoved:this.onTouchMoved,
			onTouchEnded:this.onTouchEnded});
		cc.eventManager.addListener(listener_touch, this);
		if ('mouse' in cc.sys.capabilities){
			var listener_mouse = cc.EventListener.create({
				event: cc.EventListener.MOUSE,
				swallowTouches: false,
				onMouseScroll:UpperLowerSliding.onMouseScroll
			});
			cc.eventManager.addListener(listener_mouse, this);

//			var upArrow = new Angel(this.ver, "#sel_up_arrow.png", this.down, this);
//			upArrow.setLocalZOrder(4);
//			upArrow.setPosition(this.ver.width * 0.5, this.ver.height + 20);

//			var downArrow = new Angel(this.ver, "#sel_down_arrow.png", this.up, this);
//			downArrow.setLocalZOrder(4);
//			downArrow.setPosition(this.ver.width * 0.5, 0 - 20);
		}
	},
	onTouchBegan: function(touch, event){
		var target = event.getCurrentTarget();
		var pos = cc.p(touch.getLocationX(),touch.getLocationY());
		if(cc.rectContainsPoint(
				target.getBoundingBoxToWorld(),pos)){
			this.click = true;
			return true;
		}
		return false;
	},
	onTouchMoved: function(touch, event){
		if(this.click){
			var target = event.getCurrentTarget();
			var pos = cc.p(touch.getLocationX(),touch.getLocationY());
			if(this.last == null){
				this.last = pos;
			} else {
				var margin = pos.y - this.last.y;
				var t = margin > 0 ? margin / target.cell : -margin / target.cell;
				if(t >= 1 && margin > 0){
					for(var i = 0; i < t; i++){
						target.down();
					}
					this.last = pos;
				} else if(t >= 1 && margin < 0){
					for(var i = 0; i < t; i++){
						target.up();
					}
					this.last = pos;
				}
				
			}
		}
	},
	onTouchEnded: function(touch, event){
		if(this.click){
			this.click = false;
			this.last = null;
		}
	}
});